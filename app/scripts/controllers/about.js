'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

  });
