'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:Homebelajar2Ctrl
 * @description
 * # Homebelajar2Ctrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('Homebelajar2Ctrl', function () {

    this.teknologiwebObj = [
      {
        name: 'HTML5 Boilerplate',
        tipewarna: '',
        rate: 80,
        available: true
      },
      {
        name: 'AngularJS',
        tipewarna: 'green',
        rate: 90,
        available: true
      },
      {
        name: 'Karma',
        tipewarna: 'yellow',
        rate: 78,
        available: false
      },
      {
        name: 'Express',
        tipewarna: 'teal',
        rate: 75,
        available: true
      },
      {
        name: 'Sails',
        tipewarna: 'green',
        rate: 86,
        available: false
      },
      {
        name: 'Meteors',
        tipewarna: 'green',
        rate: 98,
        available: true
      }
    ];






  });
