'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:Homebelajar2Ctrl
 * @description
 * # Homebelajar2Ctrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('Homebelajar3Ctrl', function () {

    this.teknologiwebObj = [
      {
        name: 'HTML5 Boilerplate',
        tipewarna: 'orange',
        rate: 80,
        available: true,
        imgs: 'images/ic_launcher_js.png'
      },
      {
        name: 'Angular',
        tipewarna: 'red',
        rate: 90,
        available: true,
        imgs: 'images/ic_launcher_ts.png'
      },
      {
        name: 'Aurelia',
        tipewarna: 'crimson',
        rate: 78,
        available: true,
        imgs: 'images/ic_launcher_js.png'
      },
      {
        name: 'Vue',
        tipewarna: 'teal',
        rate: 75,
        available: true,
        imgs: 'images/ic_launcher_js.png'
      },
      {
        name: 'Express',
        tipewarna: 'blue',
        rate: 86,
        available: true,
        imgs: 'images/ic_launcher_js.png'
      },
      {
        name: 'Meteor',
        tipewarna: 'pink',
        rate: 98,
        available: true,
        imgs: 'images/ic_launcher_js.png'
      }
    ];


    //hapus daftar framework
    this.removeFramework = function (itemteknoval) {

      var removedFramework = this.teknologiwebObj.indexOf(itemteknoval);
      this.teknologiwebObj.splice(removedFramework, 1);

    };

    this.addNewFramework = function () {

      var teknologiItemInsert = {
        name: this.teknologiItem.name,
        tipewarna: this.teknologiItem.tipewarna,
        rate: parseInt(this.teknologiItem.rate),
        available: true,
        imgs: ''
      };

      this.teknologiwebObj.push(teknologiItemInsert);

      this.teknologiItem.name = '';
      this.teknologiItem.tipewarna = '';
      this.teknologiItem.rate = '';
      this.teknologiItem.available = true;
    }


  });
