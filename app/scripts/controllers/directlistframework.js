'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:DirectlistframeworkCtrl
 * @description
 * # DirectlistframeworkCtrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('DirectlistframeworkCtrl', function (getListFramework) {

    var self = this;

    self.teknologiwebObj = [{
      "name": "",
      "tipewarna": "",
      "rate": 0,
      "available": true,
      "imgs": ""
    }];

    //hapus daftar framework
    self.removeFramework = function (itemteknoval) {

      var removedFramework = self.teknologiwebObj.indexOf(itemteknoval);
      self.teknologiwebObj.splice(removedFramework, 1);

    };

    self.addNewFramework = function () {

      if (!self.teknologiItem.tipewarna) {
        self.teknologiItem.tipewarna = "yellow"
      }

      var teknologiItemInsert = {
        name: self.teknologiItem.name,
        tipewarna: self.teknologiItem.tipewarna,
        rate: parseInt(self.teknologiItem.rate),
        available: true,
        imgs: 'images/ic_launcher_js.png'
      };

      self.teknologiwebObj.push(teknologiItemInsert);

      self.teknologiItem.name = '';
      self.teknologiItem.tipewarna = 'yellow';
      self.teknologiItem.rate = '';
      self.teknologiItem.available = true;
    };


    self.getDataListFrameworkServer = function () {

      var observables = Rx.Observable.create(
        function (observer) {

          getListFramework.getData().$promise.then(
            function (hasils) {

              observer.next(hasils.data);
              observer.onCompleted();
            }
          )
            .catch(function (error) {
              console.log(JSON.stringify(error));
              observer.error(error);
              observer.onCompleted();
            });
        }
      );

      observables.subscribe(
        function (results) {
          // console.log("sukses observable" + JSON.stringify(results));
          self.teknologiwebObj = results;
        },
        function (error) {
          console.log(JSON.stringify(error));
        },
        function () {
          //on complete
        }
      );

      // this.teknologiwebObj = frameworkList.get();
      // console.log(JSON.stringify(this.teknologiwebObj));

      // getListFramework.getData().$promise.then(
      //   function (hasils) {
      //
      //     //ini yang jalan
      //     self.teknologiwebObj = hasils.data;
      //
      //     //tidak jalan
      //     // this.teknologiwebObj = hasils.data;
      //   }
      // )
      //   .catch(function (error) {
      //     console.log(JSON.stringify(error));
      //     self.teknologiwebObj = [];
      //   });

      // var datapost = {
      //   mode : "json",
      //   units: "metric",
      //   cnt: "5",
      //   lat: "6.9175",
      //   lon: "107.6191"
      // };

      // postMintaCuacaBaru.postWeatherData({"APPID": '20507b6664a1b5035ecdce6829a70648'},
      //   {
      //     mode : "json",
      //     units: "metric",
      //     cnt: "5",
      //     lat: "6.9175",
      //     lon: "107.6191"
      //   }
      //   )
      //   .$promise.then(function (hasil) {
      //     console.log("data " + JSON.stringify(hasil))
      //   })
      //   .catch(function (error) {
      //     console.log("error " + JSON.stringify(error));
      //   });

      // $http({
      //   method  : 'POST',
      //   url     : 'http://api.openweathermap.org/data/2.5/forecast/daily',
      //   params  : {APPID : "20507b6664a1b5035ecdce6829a70648"},
      //   data    : {
      //     mode : "json",
      //     units: "metric",
      //     cnt: "5",
      //     lat: "6.9175",
      //     lon: "107.6191"
      //   },
      //   headers : {'Content-Type': 'application/json'}
      // }).then(function (res) {
      //
      // })
      //   .catch(function (error) {
      //
      //   });

      // $http.post('http://api.openweathermap.org/data/2.5/forecast/daily', {
      //     mode: "json",
      //     units: "metric",
      //     cnt: "5",
      //     lat: "6.9175",
      //     lon: "107.6191"
      //   }
      //   , {params: {APPID: "20507b6664a1b5035ecdce6829a70648"}},
      //   {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      //   .then(function (res) {
      //     console.log("hasil" + JSON.stringify(res))
      //   })
      //   .catch(function (error) {
      //     console.log("error" + JSON.stringify(error))
      //   });

      //metode post , di server mesti menjalankan CORS dulu dari domain lain
      // var posthasil = postFramework({customer_id: "2"}).postData({}, {
      //   "customer_id": 2,
      //   "video_id": "10"
      // }, function () {
      //   console.log("list data post " + JSON.stringify(posthasil));
      // });
      //
      // var posthasilKomentar = postKomentarVideo.postData(
      //   {},
      //   {name: 'helloPostParam', email: 'helloPostrams@mail.com'},
      //   function () {
      //     console.log("list data post sampel" + JSON.stringify(posthasilKomentar));
      //   }
      // );

    };

    self.getDataListFrameworkServer();

    self.removeAlls = function () {

      self.teknologiwebObj = [];
    }

  });
