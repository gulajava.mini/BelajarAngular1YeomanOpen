'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:HomebelajarCtrl
 * @description
 * # HomebelajarCtrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('HomebelajarCtrl', function (konstan1, $servicedata) {

    this.color = 'colorwarnates';

    this.pesanHelloWorld = 'Hello World angular 1';

    this.teknologiwebObj = [
      {
        name: 'HTML5 Boilerplate',
        tipewarna: '',
        rate: 80
      },
      {
        name: 'AngularJS',
        tipewarna: 'green',
        rate: 90
      },
      {
        name: 'Karma',
        tipewarna: 'yellow',
        rate: 78
      },
      {
        name: 'Express',
        tipewarna: 'teal',
        rate: 75
      },
      {
        name: 'Sails',
        tipewarna: 'green',
        rate: 86
      },
      {
        name: 'Meteors',
        tipewarna: 'green',
        rate: 98
      }
    ];

    this.$onInit = function () {
      console.log("on init jalan");
      console.log("isi konstan modul lain " + konstan1);

      var data = $servicedata.getData();
      console.log("hasil data servis " + data);
    };

    //jalankan fungsi on init
    this.$onInit();


  });
