'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('MainCtrl', function () {

    var self = this;

    self.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    self.datas = [];

    self.aboutSample = "About Page Works";

    self.startSampelRx = function () {

      var observables = Rx.Observable.create(
        function (observer) {
          observer.onNext('hasil');
          observer.onCompleted();
        }
      );

      observables.subscribe(
        function (results) {

          self.datas = [];
          console.log("sukses " + results);
        },
        function (error) {
          console.log(error);
        },
        function () {
        }
      )
    };

  });
