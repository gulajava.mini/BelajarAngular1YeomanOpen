'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:ContactsCtrl
 * @description
 * # ContactsCtrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('ContactsCtrl', function ($location, $window) {

    var vm = this;

    vm.sendPesanBaru = function () {

      console.log('kirim pesan');

      //navigasi di dalam app nya saja juga
      // $location.path('/contact-success');
      $location.path('contact-success');

      //navigasi ke luar situs asli
      // $window.location.href = 'http://www.google.com';

      //navigasi di dalam situs app nya saja
      // $window.location.href = 'http://localhost:9000/#/directorylist';
      // $window.location.href = '#/directorylist';

    };


  });
