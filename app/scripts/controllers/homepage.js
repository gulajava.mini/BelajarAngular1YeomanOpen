'use strict';

/**
 * @ngdoc function
 * @name belajarAngular1App.controller:HomepageCtrl
 * @description
 * # HomepageCtrl
 * Controller of the belajarAngular1App
 */
angular.module('belajarAngular1App')
  .controller('HomepageCtrl', function (getListFramework) {

    var self = this;
    self.frameworksList = [];
    self.judulHalaman = 'Random Framework';


    self.getDataFrameworkServer = function () {

      var promiseData = getListFramework.getData().$promise
        .then(function (hasils) {

          self.frameworksList = hasils.data;
        })
        .catch(function (error) {
          console.log(JSON.stringify(error));
          self.frameworksList = [];
        })
    };

    self.getDataFrameworkServer();

  });
