'use strict';

/**
 * Created by kucingmint on 2/26/17.
 */
angular.module('serviceDataNetwork')
  .factory('frameworkList', function ($resource, alamatserverGet) {

    console.log("" + alamatserverGet);
    return $resource('datajson/datajson.json', {});
  })
  .factory('getListFramework', function ($resource, alamatserverGet) {

    return $resource(alamatserverGet, {}, {
      getData: {
        method: 'GET',
        params: {}
      }
    })
  })
  .factory('postFramework', function ($resource, alamatServerPost) {

    //custom header
    return function (customHeaders) {

      return $resource(alamatServerPost, {}, {
        postData: {
          method: 'POST',
          params: {},
          data: {datapost: 'helloPost', datapostlagi: 'helloPost'},
          isArray: false,
          headers: {"Content-Type": "application/x-www-form-urlencoded"}
        }
      })
    };
  })
  .factory('postKomentarVideo', function ($resource, alamatServerPost2) {

    return $resource(alamatServerPost2, {}, {
      postData: {
        method: 'POST',
        params: {},
        data: {name: 'helloPost', email: 'helloPost@mail.com'},
        isArray: false,
        headers: {}
      }
    })
  })
  .factory('postMintaCuacaBaru', function ($resource, alamatOpenWeather) {

    return $resource(alamatOpenWeather, {APPID: ""}, {
      postWeatherData: {
        method: 'POST',
        params: {APPID: ''},
        data: {
          mode: "json",
          units: "metric",
          cnt: "5",
          lat: "6.9175",
          lon: "107.6191"
        },
        isArray: false,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    })
  });

