'use strict';

/**
 * @ngdoc overview
 * @name belajarAngular1App
 * @description
 * # belajarAngular1App
 *
 * Main module of the application.
 */
angular
  .module('belajarAngular1App', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'rx',
    'belajarAngular1App.service',
    'serviceDataNetwork'
  ])
  .config(function ($routeProvider, $locationProvider, $compileProvider) {


    $routeProvider
      .when('/homepage', {
        templateUrl: 'views/homepage.html',
        controller: 'HomepageCtrl',
        controllerAs: 'homepage'
      })
      .when('/directorylist', {
        templateUrl: 'views/directlistframework.html',
        controller: 'DirectlistframeworkCtrl',
        controllerAs: 'listfm'
      })
      .when('/contact', {
        templateUrl: 'views/contacts.html',
        controller: 'ContactsCtrl',
        controllerAs: 'contactvm'
      })
      .when('/contact-success', {
        templateUrl: 'views/contact-success.html',
        controller: 'ContactsCtrl',
        controllerAs: 'contactvm'
      })
      .when('/homebelajar3', {
        templateUrl: 'views/homebelajar3.html',
        controller: 'Homebelajar3Ctrl',
        controllerAs: 'homebelajar3'
      })
      .when('/homebelajar2', {
        templateUrl: 'views/homebelajar2.html',
        controller: 'Homebelajar2Ctrl',
        controllerAs: 'homebelajar2'
      })
      .when('/homebelajar1', {
        templateUrl: 'views/homebelajar.html',
        controller: 'HomebelajarCtrl',
        controllerAs: 'homebelajar1'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/homepage'
      });

    $locationProvider.hashPrefix("");
    // $locationProvider.html5Mode(true);

    //jadikan false ketika mode produksi
    $compileProvider.debugInfoEnabled(false);
    $compileProvider.commentDirectivesEnabled(false);
    $compileProvider.cssClassDirectivesEnabled(false);

  });
