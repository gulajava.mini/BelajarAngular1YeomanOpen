'use strict';

/**
 * @ngdoc directive
 * @name belajarAngular1App.directive:homeDetails
 * @description
 * # homeDetails
 */
angular.module('belajarAngular1App')
  .directive('homeDetails', HomeDetailDirective);

function HomeDetailDirective() {

  function homeDetailController($scope) {

    var homevm = this;
    homevm.sampeldetail = $scope.name;


  }

  return {
    restrict: 'EA',
    scope: {
      name: '=',
      datalist: '='
    },
    templateUrl: 'views/directive-homepage.html',
    link: function postLink(scope, element, attrs, $scope) {
      // element.text('this is the homeDetails directive');
      // console.log('scope isinya nama ' + scope.name + JSON.stringify(scope.datalist));
    },
    controller: homeDetailController,
    bindToController: {
      name: '=',
      datalist: '='
    },
    controllerAs: 'homevm'
  };

}
