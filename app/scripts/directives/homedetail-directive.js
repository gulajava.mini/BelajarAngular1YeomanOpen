'use strict';

/**
 * @ngdoc directive
 * @name belajarAngular1App.directive:homedetail
 * @description
 * # homedetail
 */
angular.module('belajarAngular1App')
  .directive('homeDetail', homeDetails)
  .controller('homeDetailController', homeDetailsController);

//pola nya tidak bisa bekerja dengan baik
function homeDetails() {

  function links(scope, element, attrs) {

    //   //menampilkan teks di dalam element
    //   element.text('this is the homedetail directive');
      console.log("judul link func " + scope.name + " " + scope.title);
  }

  return {
    restrict: 'E',
    scope: {
      name: '=',
      title: "="
    },
    controller: 'homeDetailController',
    controllerAs: 'vm',
    templateUrl: 'views/directive-homepage.html',
    link: links
  };
}

function homeDetailsController() {

  var homeDetailCtrl = this;

  homeDetailCtrl.datasampel = "3 saja";

  console.log('isi json = ' + homeDetailCtrl.name + " judul " + homeDetailCtrl.title + " " + homeDetailCtrl.datasampel);
}
