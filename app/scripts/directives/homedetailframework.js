'use strict';

/**
 * @ngdoc directive
 * @name belajarAngular1App.directive:homeDetailFramework
 * @description
 * # homeDetailFramework
 */
angular.module('belajarAngular1App')
  .directive('homeDetailFramework', homeDetailFuncDirective);

function homeDetailFuncDirective() {

  function homeDetailController($scope) {

    var vm = this;

    //data muncul setelah beberapa saat
    //bind to controller secara langsung akan menyambungkan scope ke dalam
    //controller "vm" , sehingga dapat langsung dipakai di tampilan
    // vm.judulHalaman = $scope.judul;
    // vm.listdataFramework = $scope.listdata;

    //isi data ada di dalam scope
    // console.log($scope.judul);
    // console.log(JSON.stringify($scope.listdata));

    //fungsi random untuk menampilkan data acak di home
    vm.randomNilai = Math.floor(Math.random() * 6);

  }

  function postLink(scope, element, attrs) {
    // element.text('this is the homeDetailFramework directive');
  }

  //bind to controller secara langsung akan menyambungkan scope ke dalam
  //controller "vm" , sehingga dapat langsung dipakai di tampilan
  return {
    restrict: 'EA',
    scope: {
      judul: '=',
      listdata: '='
    },
    templateUrl: 'views/directive-homepage.html',
    controller: homeDetailController,
    controllerAs: "vm",
    bindToController: {
      judulHalaman: '=judul',
      listdataFr: '=listdata'
    },
    link: postLink,
    transclude: true,
    replace: true
  };
}
