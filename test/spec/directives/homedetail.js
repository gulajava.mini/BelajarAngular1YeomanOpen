'use strict';

describe('Directive: homedetail', function () {

  // load the directive's module
  beforeEach(module('belajarAngular1App'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<homedetail></homedetail>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the homedetail directive');
  }));
});
