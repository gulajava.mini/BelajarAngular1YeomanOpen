'use strict';

describe('Controller: Homebelajar2Ctrl', function () {

  // load the controller's module
  beforeEach(module('belajarAngular1App'));

  var Homebelajar2Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Homebelajar2Ctrl = $controller('Homebelajar2Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Homebelajar2Ctrl.awesomeThings.length).toBe(3);
  });
});
