'use strict';

describe('Controller: HomebelajarCtrl', function () {

  // load the controller's module
  beforeEach(module('belajarAngular1App'));

  var HomebelajarCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HomebelajarCtrl = $controller('HomebelajarCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HomebelajarCtrl.awesomeThings.length).toBe(3);
  });
});
