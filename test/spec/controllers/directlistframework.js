'use strict';

describe('Controller: DirectlistframeworkCtrl', function () {

  // load the controller's module
  beforeEach(module('belajarAngular1App'));

  var DirectlistframeworkCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DirectlistframeworkCtrl = $controller('DirectlistframeworkCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DirectlistframeworkCtrl.awesomeThings.length).toBe(3);
  });
});
